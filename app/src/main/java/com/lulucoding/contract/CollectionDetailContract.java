package com.lulucoding.contract;

import com.lulucoding.model.collection.Collection;

public class CollectionDetailContract {

    public interface View { // FRAGMENT -> VIEW

        void setProgressIndicator(boolean active);

        void showCollection(Collection collection);

        void showCollectionDetailUI(String id);

        void showErrorMessage(String message);

    }

    public interface UserActionsListener { // PRESENTER -> VIEW

        void loadCollectionDetails(String id, boolean forceUpdate);

        void openCollectionDetailsUI(Collection collection);

    }
}
