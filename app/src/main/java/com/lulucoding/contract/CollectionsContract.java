package com.lulucoding.contract;

import com.lulucoding.model.collection.Collection;

import java.util.List;

public class CollectionsContract {

    public interface View { // FRAGMENT -> VIEW

        void setProgressIndicator(boolean active);

        void showCollections(List<Collection> collections);

        void showCollectionDetailUI(String id);

        void showErrorMessage(String message);

    }

    public interface UserActionsListener { // PRESENTER -> VIEW

        void loadCollections(boolean forceUpdate);

        void openCollectionDetails(Collection collection);

    }

}
