package com.lulucoding.contract;

import com.lulucoding.model.question.Question;

import java.util.List;

public class QuestionsContract {

    public interface View { // FRAGMENT -> VIEW

        void setProgressIndicator(boolean active);

        void showQuestions(Question question);

        void showQuestionDetailUI(String id);

        void showErrorMessage(String message);

    }

    public interface UserActionsListener { // PRESENTER -> VIEW

        void loadQuestions(String collectionId, boolean forceUpdate);

        void openQuestionDetails(Question question);

    }
}
