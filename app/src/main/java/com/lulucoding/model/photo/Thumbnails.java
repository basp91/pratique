package com.lulucoding.model.photo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Thumbnails {

    @SerializedName("small")
    @Expose
    private Small small;
    @SerializedName("large")
    @Expose
    private Large large;

    /**
     *
     * @return
     * The small
     */
    public Small getSmall() {
        return small;
    }

    /**
     *
     * @param small
     * The small
     */
    public void setSmall(Small small) {
        this.small = small;
    }

    /**
     *
     * @return
     * The large
     */
    public Large getLarge() {
        return large;
    }

    /**
     *
     * @param large
     * The large
     */
    public void setLarge(Large large) {
        this.large = large;
    }

}