package com.lulucoding.model.movie;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.lulucoding.model.photo.Photo;

public class Fields {

    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("Description")
    @Expose
    private String description;
    @SerializedName("Photos")
    @Expose
    private List<Photo> photos = new ArrayList<Photo>();
    @SerializedName("Seen?")
    @Expose
    private Boolean seen;
    @SerializedName("Actors")
    @Expose
    private List<String> actors = new ArrayList<String>();
    @SerializedName("Director")
    @Expose
    private List<String> director = new ArrayList<String>();
    @SerializedName("Genre")
    @Expose
    private List<String> genre = new ArrayList<String>();
    @SerializedName("Personal Rating")
    @Expose
    private String personalRating;

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The Name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The description
     */
    public String getDescription() {
        return description;
    }

    /**
     *
     * @param description
     * The Description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     *
     * @return
     * The photos
     */
    public List<Photo> getPhotos() {
        return photos;
    }

    /**
     *
     * @param photos
     * The Photos
     */
    public void setPhotos(List<Photo> photos) {
        this.photos = photos;
    }

    /**
     *
     * @return
     * The seen
     */
    public Boolean getSeen() {
        return seen;
    }

    /**
     *
     * @param seen
     * The Seen?
     */
    public void setSeen(Boolean seen) {
        this.seen = seen;
    }

    /**
     *
     * @return
     * The actors
     */
    public List<String> getActors() {
        return actors;
    }

    /**
     *
     * @param actors
     * The Actors
     */
    public void setActors(List<String> actors) {
        this.actors = actors;
    }

    /**
     *
     * @return
     * The director
     */
    public List<String> getDirector() {
        return director;
    }

    /**
     *
     * @param director
     * The Director
     */
    public void setDirector(List<String> director) {
        this.director = director;
    }

    /**
     *
     * @return
     * The genre
     */
    public List<String> getGenre() {
        return genre;
    }

    /**
     *
     * @param genre
     * The Genre
     */
    public void setGenre(List<String> genre) {
        this.genre = genre;
    }

    /**
     *
     * @return
     * The personalRating
     */
    public String getPersonalRating() {
        return personalRating;
    }

    /**
     *
     * @param personalRating
     * The Personal Rating
     */
    public void setPersonalRating(String personalRating) {
        this.personalRating = personalRating;
    }

}