package com.lulucoding.model.enums;

/**
 * Created by bea on 6/27/16.
 */
public enum PrivacyEnum {

    PUBLIC, PRIVATE, RESTRICTED
}
