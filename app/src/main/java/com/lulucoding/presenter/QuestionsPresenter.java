package com.lulucoding.presenter;

import android.support.v7.view.menu.MenuView;
import android.util.Log;

import com.lulucoding.contract.QuestionsContract;
import com.lulucoding.data.callback.QuestionsCallback;
import com.lulucoding.data.interactor.QuestionsInteractor;
import com.lulucoding.model.question.Question;

import java.util.List;

import static com.google.gson.internal.$Gson$Preconditions.checkNotNull;

public class QuestionsPresenter implements QuestionsContract.UserActionsListener, QuestionsCallback {

    private final static String TAG = "QUESTIONS_PRESENTER";

    QuestionsInteractor mQuestionsInteractor;
    QuestionsContract.View mView;


    public QuestionsPresenter(QuestionsInteractor mQuestionsInteractor, QuestionsContract.View mView) {
        this.mQuestionsInteractor = mQuestionsInteractor;
        this.mView = mView;
    }

    @Override
    public void onQuestionsLoaded(List<Question> questionList) {
        mView.setProgressIndicator(false);
        if(questionList != null && !questionList.isEmpty()){
            //mView.showQuestionDetailUI(collectionId);
        } else {
            Log.i(TAG,"No Questions");
            mView.showErrorMessage("There were no questions");
        }
    }

    @Override
    public void onFailedQuestionsLoad() {
        mView.setProgressIndicator(false);
        mView.showErrorMessage("Failed Load");
    }

    @Override
    public void loadQuestions(String collectionId, boolean forceUpdate) {
        mView.setProgressIndicator(true);
        if(forceUpdate){
            mQuestionsInteractor.refreshData();
        }
        mQuestionsInteractor.getQuestions(collectionId,this);
    }

    @Override
    public void openQuestionDetails(Question question) {
        checkNotNull(question);
        //mView.showQuestionDetailUI(question.getId());
    }
}
