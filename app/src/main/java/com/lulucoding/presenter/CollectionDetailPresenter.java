package com.lulucoding.presenter;

import android.util.Log;

import com.lulucoding.contract.CollectionDetailContract;
import com.lulucoding.data.callback.CollectionDetailsCallback;
import com.lulucoding.data.interactor.CollectionDetailInteractor;
import com.lulucoding.model.collection.Collection;

import static com.google.gson.internal.$Gson$Preconditions.checkNotNull;

public class CollectionDetailPresenter
        implements CollectionDetailContract.UserActionsListener, CollectionDetailsCallback  {

    private final static String TAG = "COLLECTION_PRESENTER";
    CollectionDetailInteractor mCollectionDetailsInteractor;
    CollectionDetailContract.View mView;

    public CollectionDetailPresenter(
            CollectionDetailInteractor mCollectionDetailsInteractor,
            CollectionDetailContract.View mView) {
        this.mCollectionDetailsInteractor = mCollectionDetailsInteractor;
        this.mView = mView;
    }

    @Override
    public void onCollectionDetailsLoaded(Collection collection) {
        mView.setProgressIndicator(false);
        if(collection != null){
            mView.showCollection(collection);
        } else {
            Log.i(TAG,"No Collection");
            mView.showErrorMessage("There were no collections");
        }
    }

    @Override
    public void onFailedCollectionDetailsLoad() {
        mView.setProgressIndicator(false);
        mView.showErrorMessage("Failed Load");
    }

    @Override
    public void onCollectionPosted(Collection collection) {

    }

    @Override
    public void onFailedCollectionPost() {

    }

    @Override
    public void loadCollectionDetails(String id, boolean forceUpdate) {
        mView.setProgressIndicator(true);
        if (forceUpdate) {
            mCollectionDetailsInteractor.refreshData();
        }
        mCollectionDetailsInteractor.getCollection(id, this);

    }

    @Override
    public void openCollectionDetailsUI(Collection collection) {
        checkNotNull(collection);
        mView.showCollectionDetailUI(collection.getId());
    }
}
