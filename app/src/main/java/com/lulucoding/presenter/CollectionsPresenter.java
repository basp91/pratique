package com.lulucoding.presenter;

import android.util.Log;

import com.lulucoding.contract.CollectionsContract;
import com.lulucoding.data.callback.CollectionsCallback;
import com.lulucoding.data.interactor.CollectionsInteractor;
import com.lulucoding.model.collection.Collection;
import com.lulucoding.pratique.R;

import java.util.List;

import static com.google.gson.internal.$Gson$Preconditions.checkNotNull;

public class CollectionsPresenter implements CollectionsContract.UserActionsListener, CollectionsCallback {

    private final static String TAG = "COLLECTIONS_PRESENTER";
    CollectionsInteractor mCollectionsInteractor;
    CollectionsContract.View mView;

    public CollectionsPresenter(CollectionsInteractor mCollectionsInteractor, CollectionsContract.View mView) {
        this.mCollectionsInteractor = mCollectionsInteractor;
        this.mView = mView;
    }

    @Override
    public void loadCollections(boolean forceUpdate) {
        mView.setProgressIndicator(true);
        if (forceUpdate) {
            mCollectionsInteractor.refreshData();
        }
        mCollectionsInteractor.getCollections(this);
    }

    @Override
    public void openCollectionDetails(Collection collection) {
        checkNotNull(collection);
        mView.showCollectionDetailUI(collection.getId());
    }

    @Override
    public void onCollectionsLoaded(List<Collection> collections) {
        mView.setProgressIndicator(false);
        if(collections != null && !collections.isEmpty()){
            mView.showCollections(collections);
        } else {
            Log.i(TAG,"No Collections");
            mView.showErrorMessage("There were no collections");
        }
    }

    @Override
    public void onFailedCollectionsLoad() {
        mView.setProgressIndicator(false);
        mView.showErrorMessage("Failed Load");
    }
}
