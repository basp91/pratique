package com.lulucoding.data.endpoint;

import com.lulucoding.model.question.Question;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface QuestionsEndpoint {

    @GET("questions")
    Call<List<Question>> getQuestions();

    @GET("questions/collection/{collection_id}")
    Call<List<Question>> getQuestions(@Path("collection_id") String collectionId);

    @GET("questions/{_id}")
    Call<Question> getQuestion(@Path("_id") String questionId);
}