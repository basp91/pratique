package com.lulucoding.data.endpoint;

import com.lulucoding.model.movie.MoviesResponse;

import retrofit2.Call;
import retrofit2.http.GET;

public interface MoviesEndpoint {

    @GET("movies")
    Call<MoviesResponse> getMovies();

}
