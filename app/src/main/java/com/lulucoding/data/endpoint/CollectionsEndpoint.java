package com.lulucoding.data.endpoint;

import com.lulucoding.model.collection.Collection;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface CollectionsEndpoint {

    @GET("collections")
    Call<List<Collection>> getCollections();
}
