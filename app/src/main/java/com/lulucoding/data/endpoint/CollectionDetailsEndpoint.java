package com.lulucoding.data.endpoint;

import com.lulucoding.model.collection.Collection;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface CollectionDetailsEndpoint {

    @GET("collections/{id}")
    Call<Collection> getCollection(@Path("id") String collectionId);

    @POST("Collections")
    void createCollection(@Body Collection collection, Call<Collection> collectionCallback);

}
