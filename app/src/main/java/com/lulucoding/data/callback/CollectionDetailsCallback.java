package com.lulucoding.data.callback;

import com.lulucoding.model.collection.Collection;
import java.util.List;


public interface CollectionDetailsCallback {

    void onCollectionDetailsLoaded(Collection collection);

    void onFailedCollectionDetailsLoad();

    void onCollectionPosted(Collection collection);

    void onFailedCollectionPost();

}
