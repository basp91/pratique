package com.lulucoding.data.callback;

import com.lulucoding.model.collection.Collection;

import java.util.List;

public interface CollectionsCallback {

    void onCollectionsLoaded(List<Collection> collections);

    void onFailedCollectionsLoad();
}
