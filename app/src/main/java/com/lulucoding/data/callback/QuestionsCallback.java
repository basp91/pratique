package com.lulucoding.data.callback;

import com.lulucoding.model.question.Question;

import java.util.List;

public interface QuestionsCallback {

    void onQuestionsLoaded(List<Question> questionList);

    void onFailedQuestionsLoad();
}
