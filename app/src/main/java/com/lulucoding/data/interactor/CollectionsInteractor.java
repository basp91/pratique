package com.lulucoding.data.interactor;

import android.util.Log;

import com.lulucoding.data.callback.CollectionsCallback;
import com.lulucoding.data.endpoint.CollectionsEndpoint;
import com.lulucoding.model.collection.Collection;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CollectionsInteractor {

    private final static String TAG = "COLLECTIONS_INTERACTOR";
    private CollectionsEndpoint mCollectionsEndpoint;

    public CollectionsInteractor(CollectionsEndpoint mCollectionsEndpoint) {
        this.mCollectionsEndpoint = mCollectionsEndpoint;
    }

    public void getCollections(final CollectionsCallback callback){
        Call<List<Collection>> call = mCollectionsEndpoint.getCollections();
        call.enqueue(new Callback<List<Collection>>() {
            @Override
            public void onResponse(Call<List<Collection>> call, Response<List<Collection>> response) {
                if(response.isSuccessful()){
                    Log.i(TAG, "success");
                    List<Collection> collectionsResponse = response.body();
                    callback.onCollectionsLoaded(collectionsResponse);
                } else {
                    callback.onFailedCollectionsLoad();
                }
            }

            @Override
            public void onFailure(Call<List<Collection>> call, Throwable t) {
                t.printStackTrace();
                Log.e(TAG, t.getMessage());
                callback.onFailedCollectionsLoad();
            }
        });
    }

    public void refreshData(){}
}
