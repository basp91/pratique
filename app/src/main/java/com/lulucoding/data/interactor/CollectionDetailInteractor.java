package com.lulucoding.data.interactor;

import android.util.Log;

import com.lulucoding.data.callback.CollectionDetailsCallback;
import com.lulucoding.data.endpoint.CollectionDetailsEndpoint;
import com.lulucoding.model.collection.Collection;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CollectionDetailInteractor {

    private final static String TAG = "COLLECTIONS_INTERACTOR";
    private CollectionDetailsEndpoint mCollectionDetailsEndpoint;

    public CollectionDetailInteractor(CollectionDetailsEndpoint mCollectionDetailsEndpoint) {
        this.mCollectionDetailsEndpoint = mCollectionDetailsEndpoint;
    }

    public void getCollection(String id, final CollectionDetailsCallback callback){
        Call<Collection> call = mCollectionDetailsEndpoint.getCollection(id);
        call.enqueue(new Callback<Collection>() {
            @Override
            public void onResponse(Call<Collection> call, Response<Collection> response) {
                if(response.isSuccessful()){
                    Log.i(TAG, "success");
                    Collection collectionResponse = response.body();
                    callback.onCollectionDetailsLoaded(collectionResponse);
                } else {
                    callback.onFailedCollectionDetailsLoad();
                }
            }

            @Override
            public void onFailure(Call<Collection> call, Throwable t) {
                t.printStackTrace();
                Log.e(TAG, t.getMessage());
                callback.onFailedCollectionDetailsLoad();
            }
        });
    }

    public void refreshData(){

    }
}
