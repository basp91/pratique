package com.lulucoding.data.interactor;

import android.util.Log;

import com.lulucoding.data.callback.QuestionsCallback;
import com.lulucoding.data.endpoint.QuestionsEndpoint;
import com.lulucoding.model.question.Question;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class QuestionsInteractor {

    private final static String TAG = "QUESTIONS_INTERACTOR";
    private QuestionsEndpoint mQuestionsEndpoint;

    public QuestionsInteractor(QuestionsEndpoint mQuestionsEndpoint) {
        this.mQuestionsEndpoint = mQuestionsEndpoint;
    }

    public void getQuestions(final String collectionId, final QuestionsCallback callback){
        Call<List<Question>> call = mQuestionsEndpoint.getQuestions(collectionId);
        call.enqueue(new Callback<List<Question>>() {
            @Override
            public void onResponse(Call<List<Question>> call, Response<List<Question>> response) {
                if(response.isSuccessful()){
                    Log.i(TAG, "success");
                    List<Question> questionsResponse = response.body();
                    callback.onQuestionsLoaded(questionsResponse);
                } else {
                    callback.onFailedQuestionsLoad();
                }
            }

            @Override
            public void onFailure(Call<List<Question>> call, Throwable t) {
                t.printStackTrace();
                Log.e(TAG, t.getMessage());
                callback.onFailedQuestionsLoad();
            }
        });
    }

    public void refreshData(){}
}
