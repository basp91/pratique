package com.lulucoding.pratique;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.mikepenz.iconics.context.IconicsContextWrapper;
import com.sromku.simple.fb.entities.Profile;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class PratiqueApplication extends Application {

    private static final String APP_PREFERENCES = "APP_PREFERENCES";

    public static final String APP_KEY_IS_LOGIN_START = "APP_KEY_IS_LOGIN_START";
    public static final String APP_VALUE_NAME = "APP_VALUE_NAME";
    public static final String APP_VALUE_LASTNAME = "APP_VALUE_LASTNAME";
    public static final String APP_VALUE_EMAIL = "APP_VALUE_EMAIL";
    public static final String APP_VALUE_PICTURE = "APP_VALUE_PICTURE";
    public static final String APP_VALUE_ID = "APP_VALUE_ID";

    private SharedPreferences mPreferences;
    private Retrofit retrofit;

    @Override
    public void onCreate() {
        super.onCreate();
        mPreferences = getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);

        Interceptor interceptor = new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request newRequest = chain.request().newBuilder()
                        .addHeader("Authorization", " "+getResources().getString(R.string.data_table_api_key)).build();
                return chain.proceed(newRequest);
            }
        };
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.interceptors().add(interceptor);
        OkHttpClient client = builder.build();

        retrofit = new Retrofit.Builder().baseUrl(getResources()
                .getString(R.string.base_api_url))
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();
    }

    // Google
    public void registerLogIn(GoogleSignInAccount profile){
        saveValuePreferences(PratiqueApplication.APP_KEY_IS_LOGIN_START, true);
        saveValuePreferences(PratiqueApplication.APP_VALUE_EMAIL,  profile.getEmail());
        saveValuePreferences(PratiqueApplication.APP_VALUE_ID, profile.getId());
        saveValuePreferences(PratiqueApplication.APP_VALUE_PICTURE, profile.getPhotoUrl().toString());
        saveValuePreferences(PratiqueApplication.APP_VALUE_NAME, profile.getDisplayName());
    }

    // Facebook
    public void registerLogIn(Profile profile){
        saveValuePreferences(PratiqueApplication.APP_KEY_IS_LOGIN_START, true);
        saveValuePreferences(PratiqueApplication.APP_VALUE_LASTNAME, profile.getLastName());
        saveValuePreferences(PratiqueApplication.APP_VALUE_NAME, profile.getName());
        saveValuePreferences(PratiqueApplication.APP_VALUE_EMAIL,  profile.getEmail());
        saveValuePreferences(PratiqueApplication.APP_VALUE_PICTURE, profile.getPicture());
        saveValuePreferences(PratiqueApplication.APP_VALUE_ID, profile.getId());
    }

    public void registerLogOut(){
        saveValuePreferences(PratiqueApplication.APP_KEY_IS_LOGIN_START, false);
        saveValuePreferences(PratiqueApplication.APP_VALUE_LASTNAME, null);
        saveValuePreferences(PratiqueApplication.APP_VALUE_NAME, null);
        saveValuePreferences(PratiqueApplication.APP_VALUE_EMAIL,  null);
        saveValuePreferences(PratiqueApplication.APP_VALUE_PICTURE, null);
        saveValuePreferences(PratiqueApplication.APP_VALUE_ID, null);
    }

    public boolean isLoginStart(){
        return getBooleanRegisterValuePreferences(PratiqueApplication.APP_KEY_IS_LOGIN_START);
    }

    public void saveValuePreferences(String key, String value){
        SharedPreferences.Editor editor = mPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public void saveValuePreferences(String key, int value){
        SharedPreferences.Editor editor = mPreferences.edit();
        editor.putInt(key, value);
        editor.commit();
    }

    public void saveValuePreferences(String key, boolean value){
        SharedPreferences.Editor editor = mPreferences.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    public String getStringRegisterValuePreferences(String key){
        return mPreferences.getString(key,null);
    }

    public boolean getBooleanRegisterValuePreferences(String key){
        return mPreferences.getBoolean(key,false);
    }

    public int getIntRegisterValuePreferences(String key){
        return mPreferences.getInt(key,0);
    }


    public SharedPreferences getPreferences() {
        return mPreferences;
    }

    public void setPreferences(SharedPreferences preferences) {
        this.mPreferences = preferences;
    }

    public Retrofit getRetrofit(){
        return retrofit;
    }

}
