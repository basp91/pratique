package com.lulucoding.util;


import android.app.Activity;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v4.app.TaskStackBuilder;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Util {

    public static void sendAndFinish(Activity activity, Class clase){
        Intent mainIntent = new Intent().setClass(activity, clase);
        activity.startActivity(mainIntent);
        activity.finish();
    }

    public static void sendTo(Activity activity, Class clase) {
        Intent mainIntent = new Intent().setClass(activity, clase);
        activity.startActivity(mainIntent);
    }

    public static void sendAndMap(Activity activity, Class clase, Map<String,String> map) {
        Intent mainIntent = new Intent().setClass(activity, clase);
        for(Map.Entry<String, String> entry : map.entrySet()){
            mainIntent.putExtra(entry.getKey(),entry.getValue());
        }
        activity.startActivity(mainIntent);
    }

    public static ProgressDialog createModalProgressDialog(Activity activity) {
        return createModalProgressDialog(activity, null);
    }

    public static ProgressDialog createModalProgressDialog(Activity activity, String dialogMessage) {
        final ProgressDialog progressDialog;
        progressDialog = new ProgressDialog(activity);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);

        if(dialogMessage != null){
            progressDialog.setMessage(dialogMessage);
        }
        return progressDialog;
    }
}
