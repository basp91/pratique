package com.lulucoding.util;

import android.content.Context;

import com.lulucoding.data.endpoint.CollectionsEndpoint;
import com.lulucoding.data.interactor.CollectionsInteractor;
import com.lulucoding.pratique.PratiqueApplication;

public class Injection {

    public static CollectionsInteractor provideMoviesInteractor(Context context){
        return new CollectionsInteractor(provideMoviesApiService(context));
    }

    public static CollectionsEndpoint provideMoviesApiService(Context context){
        PratiqueApplication app = (PratiqueApplication) context.getApplicationContext();
        return app.getRetrofit().create(CollectionsEndpoint.class);
    }

}
