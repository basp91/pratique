package com.lulucoding.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.lulucoding.model.collection.Collection;
import com.lulucoding.view.fragment.CollectionsFragment;
import com.lulucoding.pratique.R;

import java.util.List;

import static com.google.gson.internal.$Gson$Preconditions.checkNotNull;

public class CollectionsAdapter extends RecyclerView.Adapter<CollectionsAdapter.ViewHolder> {

    private List<Collection> mCollections;
    private Context mContext;
    private CollectionsFragment.CollectionItemListener mListener;
    private static final String TAG = "MY_COLLECTION_RECYCLER";

    public CollectionsAdapter(Context context, List<Collection> items, CollectionsFragment.CollectionItemListener listener) {
        Log.i(TAG,"CollectionsAdapter");
        setList(items);
        mContext = context;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.i(TAG,"onCreateViewHolder");
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_collection, parent, false);
        return new ViewHolder(view, mListener);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        Log.i(TAG,"onBindViewHolder");
        holder.mItem = mCollections.get(position);
        holder.mCollectionNameView.setText(mCollections.get(position).getName());
        holder.mCollectionSubjectView.setText(mCollections.get(position).getSubject());
        holder.mCollectionDescriptionView.setText(mCollections.get(position).getInfo());
        holder.mCollectionQuestionsView.setText(mCollections.get(position).getPrivacy());

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG,"onBindViewHolder.onClick");
                if (null != mListener) {
                    mListener.onCollectionInteraction(holder.mItem);
                }
            }
        });


    }

    public void replaceData(List<Collection> collectionList) {
        setList(collectionList);
        notifyDataSetChanged();
    }

    private void setList(List<Collection> collectionList) {
        mCollections = checkNotNull(collectionList);
    }


    @Override
    public int getItemCount() {
        Log.i(TAG,"getItemCount");
        return mCollections.size();
    }

    public Collection getItem(int position) {
        return mCollections.get(position);
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mCollectionNameView;
        public final TextView mCollectionSubjectView;
        public final TextView mCollectionDescriptionView;
        public final TextView mCollectionQuestionsView;
        public Collection mItem;
        private CollectionsFragment.CollectionItemListener mItemListener;


        public ViewHolder(View view, CollectionsFragment.CollectionItemListener listener) {
            super(view);
            Log.i(TAG,"ViewHolder");
            mView = view;
            mItemListener = listener;
            mCollectionNameView = (TextView) view.findViewById(R.id.collection_name_text_view);
            mCollectionSubjectView = (TextView) view.findViewById(R.id.collection_subject_text_view);
            mCollectionDescriptionView = (TextView) view.findViewById(R.id.collection_description_text_view);
            mCollectionQuestionsView = (TextView) view.findViewById(R.id.collection_questions_text_view);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mCollectionSubjectView.getText() + "'";
        }
    }
}
