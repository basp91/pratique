package com.lulucoding.view.fragment;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment; // SIEMPRE USAR LA DE SUPPORT PARA QUE FUNCIONE EN VARIAS VERSIONES
import android.widget.DatePicker;

public class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

    private NoticeDialogListener mListener;

    public static DatePickerFragment newInstance(Long dateLimit, int year, int month, int day){
        DatePickerFragment fragment = new DatePickerFragment();
        Bundle args = new Bundle();
        args.putInt("year", year);
        args.putInt("month", month);
        args.putInt("day", day);
        args.putLong("dateLimit", dateLimit);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        int year = getArguments().getInt("year");
        int month = getArguments().getInt("month");
        int day = getArguments().getInt("day");
        Long dateLimit = getArguments().getLong("dateLimit");
        DatePickerDialog dpd =  new DatePickerDialog(getActivity(), this, year, month, day);
        if(dateLimit!=null)
            dpd.getDatePicker().setMinDate(dateLimit);
        return dpd;
    }

//    @Override
//    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
        //String texto = "Es el año "+ year + " mes "+ month + " y día " + day;
        //Toast.makeText(datePicker.getContext(), texto, Toast.LENGTH_SHORT).show();
//    }

    @Override
    public void onAttach(Activity activity){
        super.onAttach(activity);
        try {
            mListener = (NoticeDialogListener) activity;
        } catch (ClassCastException e)  {
            throw new ClassCastException(activity.toString() + "no pusiste el NoticeDialogListener");
        }
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int day) {
        mListener.onDateSet(view, year, month, day);
    }

    public interface NoticeDialogListener {
        void onDateSet(DatePicker view, int year, int month, int day);
    }

}
