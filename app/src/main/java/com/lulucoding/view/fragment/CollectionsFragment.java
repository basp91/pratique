package com.lulucoding.view.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.lulucoding.contract.CollectionsContract;
import com.lulucoding.data.endpoint.CollectionsEndpoint;
import com.lulucoding.model.collection.Collection;
import com.lulucoding.presenter.CollectionsPresenter;
import com.lulucoding.util.Injection;
import com.lulucoding.util.Util;
import com.lulucoding.view.activity.NewCollectionActivity;
import com.lulucoding.view.activity.QuestionActivity;
import com.lulucoding.view.adapter.CollectionsAdapter;
import com.lulucoding.pratique.PratiqueApplication;
import com.lulucoding.pratique.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link CollectionItemListener}
 * interface.
 */
public class CollectionsFragment extends Fragment implements CollectionsContract.View {

    private static final String TAG = "COLLECTION_FRAG";
    private static final String ARG_COLUMN_COUNT = "ARG_COLUMN_COUNT";

    private int mColumnCount = 1;
    private RecyclerView mRecyclerView;

    private CollectionsContract.UserActionsListener mActionsListener;
    private ProgressDialog mProgressDialog;
    private CollectionsAdapter mAdapter;

    public CollectionsFragment() {}

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static CollectionsFragment newInstance(int columnCount) {
        Log.i(TAG,"newInstance");
        CollectionsFragment fragment = new CollectionsFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG,"onCreate");

        mAdapter = new CollectionsAdapter(getContext(), new ArrayList<Collection>(0), new CollectionItemListener() {
            @Override
            public void onCollectionInteraction(Collection item) {
                mActionsListener.openCollectionDetails(item);
            }
        });
        mActionsListener = new CollectionsPresenter(Injection.provideMoviesInteractor(getContext()), this);
        mProgressDialog = Util.createModalProgressDialog(getActivity());

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
        setHasOptionsMenu(true);
    }

    @Override
    public void onResume() {
        super.onResume();
        mActionsListener.loadCollections(false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.i(TAG,"onCreateView");
        View view = inflater.inflate(R.layout.fragment_collection_list, container, false);

        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            mRecyclerView = (RecyclerView) view;
            if (mColumnCount <= 1) {
                mRecyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                mRecyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }
        }
        loadFromServer();
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setRetainInstance(true);
    }

    private void loadFromServer(){
        PratiqueApplication app = (PratiqueApplication) this.getActivity().getApplicationContext();
        CollectionsEndpoint apiService = app.getRetrofit().create(CollectionsEndpoint.class);

        Call<List<Collection>> call = apiService.getCollections();
        call.enqueue(new Callback<List<Collection>>() {
            @Override
            public void onResponse(Call<List<Collection>> call, Response<List<Collection>> response) {
                Log.i(TAG,"onResponse");
                List<Collection> collectionsResponse = response.body();
                loadCollections(collectionsResponse);
            }

            @Override
            public void onFailure(Call<List<Collection>> call, Throwable t) {
                t.printStackTrace();
                Log.e(TAG,t.getMessage());
            }
        });
    }

    private void loadCollections(List<Collection> collectionList){
        Log.i(TAG,"loadCollections");
        mRecyclerView.setAdapter(mAdapter);
        Log.i(TAG,collectionList.toString());
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.i(TAG,"onDetach");
        mActionsListener = null;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.findItem(R.id.action_settings).setVisible(false);
        inflater.inflate(R.menu.menu_new_collection, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.menu_action_item_new_collection){
            Util.sendAndFinish(this.getActivity(), NewCollectionActivity.class);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void setProgressIndicator(final boolean active) {
        if(active){
            mProgressDialog.setMessage("Cargando colecciones");
            mProgressDialog.show();
        } else {
            mProgressDialog.dismiss();
        }
    }

    @Override
    public void showCollections(List<Collection> collections) {
        mAdapter.replaceData(collections);
    }

    @Override
    public void showCollectionDetailUI(String collectionId) {
        HashMap<String,String> map = new HashMap<String,String>();
        map.put("COLLECTION_ID",collectionId);
        Util.sendAndMap(this.getActivity(), QuestionActivity.class, map);
    }

    @Override
    public void showErrorMessage(String message) {
        Snackbar.make(getView(), message, Snackbar.LENGTH_SHORT).show();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     */
    public interface CollectionItemListener {
        // TODO: Update argument type and name
        void onCollectionInteraction(Collection item);
    }


}
