package com.lulucoding.view.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.lulucoding.contract.QuestionsContract;
import com.lulucoding.data.endpoint.QuestionsEndpoint;
import com.lulucoding.model.question.Question;
import com.lulucoding.pratique.PratiqueApplication;
import com.lulucoding.pratique.R;
import com.lulucoding.view.activity.MainActivity;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link QuestionItemListener} interface
 * to handle interaction events.
 * Use the {@link QuestionFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class QuestionFragment extends Fragment implements QuestionsContract.View {

    private static final String ARG_QUESTION_ID = "question_id";
    private static final Question ARG_QUESTION = null;
    private static final String TAG = "QUESTION_FRAGMENT";

    private String mQuestionId;
    private List<Question> mQuestionList = new ArrayList<Question>();

    private QuestionsContract.UserActionsListener mActionsListener;

    private FragmentManager mFragmentManager;
    private FragmentTransaction mFragmentTransaction;
    private ProgressDialog mProgressDialog;

    private static QuestionFragment mFragment;
    private static Bundle mArgs;

    public QuestionFragment() {}

    public static QuestionFragment newInstance(Question question) {
        Log.i(TAG,"newInstance");
        QuestionFragment fragment = new QuestionFragment();
        Bundle args = new Bundle();
//        args.putString(ARG_QUESTION_ID, mQuestionId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.i(TAG,"onCreate");
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mQuestionId = getArguments().getString(ARG_QUESTION_ID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Log.i(TAG,"onCreateView");
        ((MainActivity) getContext()).getSupportActionBar().hide();
        //return inflater.inflate(R.layout.fragment_question, container, false);
        return (ViewGroup) inflater.inflate(R.layout.fragment_question, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Question question) {
        Log.i(TAG,"onButtonPressed");
        if (mActionsListener != null) {
            mActionsListener.openQuestionDetails(question);
        }
    }

   /* @Override
    public void onAttach(Context context) {
        Log.i(TAG,"onAttach");
        super.onAttach(context);
        if (context instanceof QuestionItemListener) {
            mActionsListener = (QuestionItemListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement QuestionItemListener");
        }
    }*/

    @Override
    public void onDetach() {
        super.onDetach();
        mActionsListener = null;
        ((MainActivity) getContext()).getSupportActionBar().show();
    }

    @Override
    public void setProgressIndicator(boolean active) {
        if(active){
            mProgressDialog.setMessage("Cargando preguntas");
            mProgressDialog.show();
        } else {
            mProgressDialog.dismiss();
        }
    }

    @Override
    public void showQuestions(Question question) {
        Log.i(TAG,"showQuestions");
        //
    }

    @Override
    public void showQuestionDetailUI(String id) {
        Log.i(TAG,"showQuestionDetailUI");

    }

    @Override
    public void showErrorMessage(String message) {
        Snackbar.make(getView(), message, Snackbar.LENGTH_SHORT).show();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     */
    public interface QuestionItemListener {
        // TODO: Update argument type and name
        void onQuestionInteraction(Question question);
    }
}
