package com.lulucoding.view.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.util.Log;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.lulucoding.pratique.PratiqueApplication;
import com.lulucoding.pratique.R;
import com.lulucoding.util.Util;
import com.sromku.simple.fb.Permission;
import com.sromku.simple.fb.SimpleFacebook;
import com.sromku.simple.fb.SimpleFacebookConfiguration;
import com.sromku.simple.fb.entities.Profile;
import com.sromku.simple.fb.listeners.OnLoginListener;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.sromku.simple.fb.listeners.OnLogoutListener;
import com.sromku.simple.fb.listeners.OnProfileListener;

import java.util.List;

public class LoginActivity
        extends     AppCompatActivity
        implements  View.OnClickListener,
                    GoogleApiClient.OnConnectionFailedListener {

    // VARIABLES

    private PratiqueApplication app;
    private static final String TAG = "LOG_IN_ACTIVITY";

    private static final int GOOGLE_RC_SIGN_IN = 9001;
    private static final int FB_RC_SIGN_IN = 64206;

    private TextView mStatusTextView;
    private ProgressDialog mProgressDialog;

    private SimpleFacebook mSimpleFacebook;
    private GoogleApiClient mGoogleApiClient;

    // METHODS

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(TAG,"onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // APP GLOBAL
        app = (PratiqueApplication) getApplication();

        // LAYOUT
        mStatusTextView = (TextView) findViewById(R.id.login_status);

        // BUTTON LISTENERS
        findViewById(R.id.google_sign_in_button).setOnClickListener(this);
        findViewById(R.id.facebook_sign_in_button).setOnClickListener(this);
        /*findViewById(R.id.google_sign_out_button).setOnClickListener(this);
        findViewById(R.id.google_disconnect_button).setOnClickListener(this);
        findViewById(R.id.facebook_sign_out_button).setOnClickListener(this);*/

        // GOOGLE CONFIGURATION -->
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestId()
                .requestProfile()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        // FACEBOOK CONFIGURATION -->
        SimpleFacebookConfiguration configuration = new SimpleFacebookConfiguration.Builder()
                .setAppId(getResources().getString(R.string.facebook_app_id))
                .setNamespace("lulucoding_pratique")
                .setPermissions(new Permission[] {
                        Permission.EMAIL,
                })
                .build();
        SimpleFacebook.setConfiguration(configuration);
    }

    @Override
    public void onClick(View v) {
        Log.i(TAG,"onClick");
        switch (v.getId()) {
            case R.id.google_sign_in_button:            // LOG IN
                Log.i(TAG, "GOOGLE LOG IN TRIGGERED");
                googleSignIn();
                break;
            case R.id.facebook_sign_in_button:          // LOG IN
                Log.i(TAG, "FACEBOOK LOG IN TRIGGERED");
                facebookSignIn(v);
                break;
            // LOG OUT
            /*
            case R.id.google_sign_out_button:
                googleSignOut();
                Log.i(TAG, "GOOGLE LOG OUT TRIGGERED");
                break;
            case R.id.facebook_sign_out_button:
                facebookSignOut(v);
                Log.i(TAG, "FACEBOOK LOG OUT TRIGGERED");
                break;
            // DISCONNECT
            case R.id.google_disconnect_button:
                googleRevokeAccess();
                break;*/
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.i(TAG,"onActivityResult");
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == FB_RC_SIGN_IN) {
            mSimpleFacebook.onActivityResult(requestCode, resultCode, data);
        }

        if (requestCode == GOOGLE_RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            Log.d(TAG,"onActivityResult: " + result.isSuccess());
            handleGoogleSignInResult(result);
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.e(TAG, "onConnectionFailed =" + connectionResult);
    }

    @Override
    public void onResume() {
        Log.i(TAG,"onResume");
        super.onResume();
        mSimpleFacebook = SimpleFacebook.getInstance(this);

        OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(mGoogleApiClient);
        if (opr.isDone()) {
            Log.d(TAG, "onResume: Google got cached sign-in");
            GoogleSignInResult result = opr.get();
            handleGoogleSignInResult(result);
        } else {
            Log.d(TAG, "onResume: Google un-cached sign-in");
            showProgressDialog();
            opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                @Override
                public void onResult(GoogleSignInResult googleSignInResult) {
                    hideProgressDialog();
                    handleGoogleSignInResult(googleSignInResult);
                }
            });
        }
    }

    /*
    @Override
    public void onStart() {
        super.onStart();
    }
    */

    private void googleSignIn() {
        Log.i(TAG,"googleSignIn");
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, GOOGLE_RC_SIGN_IN);
    }

    private void handleGoogleSignInResult(GoogleSignInResult result) {
        Log.d(TAG, "handleGoogleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {       // Signed in successfully
            GoogleSignInAccount acct = result.getSignInAccount();
            mStatusTextView.setText(getString(R.string.signed_in_fmt, acct.getDisplayName()));
            //updateUI(true);
            app.registerLogIn(acct);
            Util.sendAndFinish(LoginActivity.this,MainActivity.class);
        } else {                        // Signed out
            updateUI(false);
        }
    }

    private void googleSignOut() {
        Log.i(TAG,"googleSignOut");

        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        app.registerLogOut();
                        updateUI(false);
                    }
                });
    }

    private void googleRevokeAccess() {
        Log.i(TAG,"googleRevokeAccess");

        Auth.GoogleSignInApi.revokeAccess(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        app.registerLogOut();
                        updateUI(false);
                    }
                });
    }

    private void showProgressDialog() {
        Log.i(TAG,"showProgressDialog");

        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage(getString(R.string.loading));
            mProgressDialog.setIndeterminate(true);
        }
        mProgressDialog.show();
    }

    private void hideProgressDialog() {
        Log.i(TAG,"hideProgressDialog");
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.hide();
        }
    }

    private void updateUI(boolean signedIn) {
        Log.i(TAG,"updateUI: SignedIn "+ signedIn);

        if (signedIn) {
            findViewById(R.id.google_sign_in_button).setVisibility(View.GONE);
            findViewById(R.id.facebook_sign_in_button).setVisibility(View.GONE);
            /*
            findViewById(R.id.google_sign_out_and_disconnect).setVisibility(View.VISIBLE);

            if(app.getLoggedInBy() == GOOGLE_RC_SIGN_IN) {
                findViewById(R.id.google_disconnect_button).setVisibility(View.VISIBLE);
                findViewById(R.id.google_sign_out_button).setVisibility(View.VISIBLE);
            } else if (app.getLoggedInBy() == FB_RC_SIGN_IN){
                findViewById(R.id.facebook_sign_out_button).setVisibility(View.VISIBLE);
            }*/

        } else {
            mStatusTextView.setText(R.string.signed_out);
            findViewById(R.id.google_sign_in_button).setVisibility(View.VISIBLE);
            findViewById(R.id.facebook_sign_in_button).setVisibility(View.VISIBLE);
            /*
            findViewById(R.id.google_sign_out_and_disconnect).setVisibility(View.GONE);

            if(app.getLoggedInBy() == GOOGLE_RC_SIGN_IN) {
                findViewById(R.id.google_disconnect_button).setVisibility(View.GONE);
                findViewById(R.id.google_sign_out_button).setVisibility(View.GONE);
            } else if (app.getLoggedInBy() == FB_RC_SIGN_IN){
                findViewById(R.id.facebook_sign_out_button).setVisibility(View.GONE);
            }
            */
        }

    }


    private void facebookSignIn(View view){
        Log.i(TAG,"facebookSignIn");
        mSimpleFacebook.login(onFacebookLoginListener);
    }

    private void facebookSignOut(View view) {
        Log.i(TAG,"facebookSignOut");
        mSimpleFacebook.logout(onFacebookLogoutListener);
    }

    // FACEBOOK LISTENERS

    private OnLoginListener onFacebookLoginListener = new OnLoginListener() {

        @Override
        public void onLogin(String accessToken, List<Permission> acceptedPermissions, List<Permission> declinedPermissions) {
            Log.i(TAG,"onFacebookLoginListener.onLogin");

            Profile.Properties properties = new Profile.Properties.Builder()
                    .add(Profile.Properties.ID)
                    .add(Profile.Properties.NAME)
                    .add(Profile.Properties.EMAIL)
                    .add(Profile.Properties.PICTURE)
                    .add(Profile.Properties.LAST_NAME)
                    .build();
            mSimpleFacebook.getProfile(properties, onFacebookProfileListener);
            mStatusTextView.setText(getString(R.string.signed_in_fmt, Profile.Properties.NAME.toString()));
        }

        @Override
        public void onCancel() {
            Log.i(TAG,"onFacebookLoginListener.onCancel");
        }

        @Override
        public void onFail(String reason) {
            Log.e(TAG,"onFacebookLoginListener.onFail: " + reason);
        }

        @Override
        public void onException(Throwable throwable) {
            Log.i(TAG, "onFacebookLoginListener.onException: " + throwable.toString());
        }

    };

    private OnProfileListener onFacebookProfileListener = new OnProfileListener() {
        @Override
        public void onComplete(Profile profile) {
            Log.i(TAG,"onFacebookProfileListener.onComplete");
            app.registerLogIn(profile);
            Log.i(TAG, "onFacebookProfileListener: PROFILE ID = " + profile.getId());
            Util.sendAndFinish(LoginActivity.this, MainActivity.class);
        }

        @Override
        public void onThinking(){
            Log.i(TAG,"onFacebookProfileListener.onThinking");
        }

        @Override
        public void onFail(String reason){
            Log.e(TAG, "onFacebookProfileListener.onFail: " + reason);
        }

        @Override
        public void onException(Throwable throwable){
            super.onException(throwable);
            Log.i(TAG,"onFacebookProfileListener.onException");
            Log.i(TAG, "onFacebookProfileListener.onException: " + throwable.toString());
        }

    };

    private OnLogoutListener onFacebookLogoutListener = new OnLogoutListener() {

        @Override
        public void onLogout() {
            Log.i(TAG,"onFacebookLogoutListener.onLogout");
            app.registerLogOut();
            Log.i(TAG, "onFacebookLogoutListener.onLogout: LOGGED OUT");
        }
    };

}