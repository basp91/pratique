package com.lulucoding.view.activity;

import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.lulucoding.view.fragment.DatePickerFragment;
import com.lulucoding.view.fragment.TimePickerFragment;
import com.lulucoding.pratique.R;

public class CheckboxActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    //private HashMap<Integer, Boolean> animales;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkbox);

        Spinner spinner = (Spinner) findViewById(R.id.spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.cities, android.R.layout.simple_spinner_dropdown_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);

//        animales.put(R.id.checkboxCat,false);
//        animales.put(R.id.checkboxDog,false);
//        animales.put(R.id.checkboxFish,false);
    }
/*
    public void selectAnimal(View view){
        boolean checked = ((CheckBox) view).isChecked();
        switch (view.getId()){
            case R.id.checkboxCat:
                animales.put(R.id.checkboxCat,checked);
                break;
            case R.id.checkboxDog:
                animales.put(R.id.checkboxDog,checked);
                break;
            case R.id.checkboxFish:
                animales.put(R.id.checkboxFish,checked);
                break;
        }
    }
*/
    public void showGender(View view)
    {
        switch (view.getId()){
            case R.id.radioFemale:
                showMessage(getResources().getString(R.string.female));
                break;
            case R.id.radioMale:
                showMessage(getResources().getString(R.string.male));
                break;
        }
    }

    private void showMessage(String answer){
        String message;
        message = " I am a " + answer;
        String message2 = "I like ";
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        Toast.makeText(this, message2, Toast.LENGTH_SHORT).show();

    }

    private void inicializar(){
        ToggleButton toggle = (ToggleButton) findViewById(R.id.toggleButton);
        toggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    // The toggle is enabled
                } else {
                    // The toggle is disabled
                }
            }
        });
        toggle.isChecked();

        Switch switchs = (Switch) findViewById(R.id.switch1);
        switchs.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    // The toggle is enabled
                } else {
                    // The toggle is disabled
                }
            }
        });
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        String mensaje = "Seleccionaste "+getResources().getStringArray(R.array.cities)[i];
        Toast.makeText(this,mensaje,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    public void showDatePickerDialog(View v) {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getSupportFragmentManager(), "DATEPICKER");
    }

    public void showTimePickerDialog(View v) {
        DialogFragment newFragment = new TimePickerFragment();
        newFragment.show(getSupportFragmentManager(), "TIMEPICKER");
    }

}
