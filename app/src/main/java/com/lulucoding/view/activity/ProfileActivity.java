package com.lulucoding.view.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.lulucoding.pratique.R;

public class ProfileActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
    }
}
