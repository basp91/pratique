package com.lulucoding.view.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;

import com.lulucoding.data.endpoint.QuestionsEndpoint;
import com.lulucoding.model.question.Question;
import com.lulucoding.pratique.PratiqueApplication;
import com.lulucoding.pratique.R;
import com.lulucoding.util.transform.ZoomOutPageTransform;
import com.lulucoding.view.fragment.QuestionFragment;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.google.gson.internal.$Gson$Preconditions.checkNotNull;

public class QuestionActivity extends FragmentActivity implements QuestionFragment.QuestionItemListener{

    private static final String TAG = "QUESTION_ACTIVITY";
    private static final int NUM = 2;
    private static final String ARG_COLLECTION_ID = "COLLECTION_ID";

    private String mCollectionId = "collection_id";
    private List<Question> mQuestionList;

    private ViewPager mQuestionPager;
    private PagerAdapter mQeustionPagerAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(TAG,"onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_question_slider);

        mCollectionId = (String) getIntent().getExtras().get(ARG_COLLECTION_ID);
        loadFromServer(mCollectionId);

        mQuestionPager = (ViewPager) findViewById(R.id.fragment_question_slider);
        mQuestionPager.setPageTransformer(true, new ZoomOutPageTransform());
        mQeustionPagerAdapter = new QuestionPagerAdapter(getSupportFragmentManager());
        mQuestionPager.setAdapter(mQeustionPagerAdapter);

        /*mFragmentManager = getSupportFragmentManager();
            mFragmentTransaction = mFragmentManager.beginTransaction();
            QuestionFragment mFragment = QuestionFragment.newInstance(id);
            mFragmentTransaction.replace(R.id.container,mFragment);
            mFragmentTransaction.addToBackStack(null);
            mFragmentTransaction.commit();*/
    }

    @Override
    public void onBackPressed() {
        Log.i(TAG,"onBackPressed");
        if (mQuestionPager.getCurrentItem() == 0) {
            // If the user is currently looking at the first step, allow the system to handle the
            // Back button. This calls finish() on this activity and pops the back stack.
            super.onBackPressed();
        } else {
            // Otherwise, select the previous step.
            mQuestionPager.setCurrentItem(mQuestionPager.getCurrentItem() - 1);
        }
    }

    @Override
    public void onQuestionInteraction(Question question) {

    }

    private void loadFromServer(String collectionId) {
        Log.i(TAG,"loadFromServer");
        PratiqueApplication app = (PratiqueApplication) getApplicationContext();
        QuestionsEndpoint apiService = app.getRetrofit().create(QuestionsEndpoint.class);

        Call<List<Question>> call = apiService.getQuestions(collectionId);
        call.enqueue(new Callback<List<Question>>() {
            @Override
            public void onResponse(Call<List<Question>> call, Response<List<Question>> response) {
                Log.i(TAG,"onResponse");
                List<Question> questionResponse = response.body();
                Log.i(TAG,questionResponse.toString());
                loadQuestions(questionResponse);
            }

            @Override
            public void onFailure(Call<List<Question>> call, Throwable t) {
                t.printStackTrace();
                Log.e(TAG,t.getMessage());
            }
        });
    }

    private void loadQuestions(List<Question> questionList) {
        Log.i(TAG,"loadQuestions");
        mQuestionList = checkNotNull(questionList);
    }

    /**
     * A simple pager adapter that represents objects, in sequence.
     */
    private class QuestionPagerAdapter extends FragmentStatePagerAdapter {

        //private List<Question> mQuestionList;
        private static final String TAG = "QUESTION_ADAPTER";

        public QuestionPagerAdapter(FragmentManager fm) {
            super(fm);
            Log.i(TAG,"QuestionPagerAdapter");
        }

        @Override
        public Fragment getItem(int position) {
            Log.i(TAG,"getItem");
            Question item = mQuestionList.get(position);
            return QuestionFragment.newInstance(item);
        }

        @Override
        public int getCount() {
            Log.i(TAG,"getCount");
            //return mQuestionList.size();
            return NUM;
        }

    }
}
