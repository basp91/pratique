package com.lulucoding.view.activity;


import android.content.Context;
import android.net.Uri;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Menu;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.lulucoding.model.collection.Collection;
import com.lulucoding.model.question.Question;
import com.lulucoding.view.fragment.CollectionsFragment;
import com.lulucoding.view.fragment.DatePickerFragment;
import com.lulucoding.pratique.PratiqueApplication;
import com.lulucoding.pratique.R;
import com.lulucoding.util.transform.CircleTransform;
import com.lulucoding.util.Util;
import com.lulucoding.view.fragment.QuestionFragment;
import com.mikepenz.iconics.context.IconicsContextWrapper;
import com.squareup.picasso.Picasso;

public class MainActivity
        extends AppCompatActivity
        implements  NavigationView.OnNavigationItemSelectedListener,
                    DatePickerFragment.NoticeDialogListener,
                    CollectionsFragment.CollectionItemListener,
                    QuestionFragment.QuestionItemListener {

    PratiqueApplication app;
    private FragmentManager mFragmentManager;
    private FragmentTransaction mFragmentTransaction;
    private Fragment mFragment;
    private static final String TAG = "MAIN_ACTIVITY";

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            Log.i(TAG,"onCreate");
            setContentView(R.layout.activity_main);

            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            app = (PratiqueApplication) getApplicationContext();

            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                    this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            drawer.setDrawerListener(toggle);
            toggle.syncState();

            NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
            navigationView.setNavigationItemSelectedListener(this);

            View headerView = navigationView.getHeaderView(0);
            ImageView profileImageView = (ImageView) headerView.findViewById(R.id.nav_profile_image_view);
            TextView nameTextView = (TextView) headerView.findViewById(R.id.nav_profile_name_text_view);
            TextView emailTextView = (TextView) headerView.findViewById(R.id.nav_profile_email_text_view);

            String name = app.getStringRegisterValuePreferences(PratiqueApplication.APP_VALUE_NAME);
            String email = app.getStringRegisterValuePreferences(PratiqueApplication.APP_VALUE_EMAIL);
            String picture = app.getStringRegisterValuePreferences(PratiqueApplication.APP_VALUE_PICTURE);

            nameTextView.setText(name);
            emailTextView.setText(email);

            Picasso.with(this).load(picture)
                .transform(new CircleTransform())
                    .resize(200,200)
                    .into(profileImageView);
        }

        @Override
        public void onBackPressed() {
            Log.i(TAG,"onBackPressed");

            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
            } else {
                super.onBackPressed();
            }
        }

        @Override
        public boolean onCreateOptionsMenu(Menu menu) {
            Log.i(TAG,"onCreateOptionsMenu");
            // Inflate the menu; this adds items to the action bar if it is present.
            getMenuInflater().inflate(R.menu.main, menu);
            return true;
        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            Log.i(TAG,"onOptionsItemSelected");

            // Handle action bar item clicks here. The action bar will
            // automatically handle clicks on the Home/Up button, so long
            // as you specify a parent activity in AndroidManifest.xml.
            int id = item.getItemId();
            switch (id){
                case R.id.action_settings:
                    Toast.makeText(this, "Shi hizo click", Toast.LENGTH_SHORT).show();
                    break;
            }
            return super.onOptionsItemSelected(item);
        }

        @SuppressWarnings("StatementWithEmptyBody")
        @Override
        public boolean onNavigationItemSelected(MenuItem item) {
            Log.i(TAG,"onNavigationItemSelected");

            int id = item.getItemId();

            switch (id){
                case R.id.nav_collections:
                    mFragmentManager = getSupportFragmentManager();
                    mFragmentTransaction = mFragmentManager.beginTransaction();
                    mFragment = CollectionsFragment.newInstance(1);
                    mFragmentTransaction.replace(R.id.container,mFragment);
                    mFragmentTransaction.addToBackStack(null);
                    mFragmentTransaction.commit();
                    break;
                case R.id.nav_browse:
                    break;
                case R.id.nav_settings:
                    break;
                case R.id.nav_share:
                    break;
                case R.id.nav_feedback:
                    break;
            }

            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            drawer.closeDrawer(GravityCompat.START);
            return true;
        }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int day) {
        Log.i(TAG,"onDateSet");
        Toast.makeText(view.getContext(), year+" "+month+" "+day, Toast.LENGTH_SHORT).show();
    }

    public void newQuestionGroup(View view){
        Log.i(TAG,"newQuestionGroup");
        Util.sendAndFinish(MainActivity.this, NewCollectionActivity.class);
    }
    public void goToProfile(View view){
        Util.sendAndFinish(MainActivity.this, ProfileActivity.class);
    }

    @Override
    public void onCollectionInteraction(Collection item) {
        Log.i(TAG,"onCollectionInteraction");
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(IconicsContextWrapper.wrap(newBase));
    }

    @Override
    public void onQuestionInteraction(Question question) {

    }
}
