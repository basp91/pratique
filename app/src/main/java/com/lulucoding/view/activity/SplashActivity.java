package com.lulucoding.view.activity;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.lulucoding.pratique.PratiqueApplication;
import com.lulucoding.pratique.R;
import com.lulucoding.util.Util;

public class SplashActivity extends AppCompatActivity {
    private PratiqueApplication app;
    private static final String TAG = "SPLASH_ACTIVITY";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG,"onCreate");
        setContentView(R.layout.activity_splash);

        app = (PratiqueApplication) getApplicationContext();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(app.isLoginStart()){
                    Log.i(TAG,"Handler().postDelayed.run: isLoginStart? true");
                    Util.sendAndFinish(SplashActivity.this, MainActivity.class);
                    //Util.sendAndFinish(SplashActivity.this, LoginActivity.class);
                }else{
                    Log.i(TAG,"Handler().postDelayed.run: isLoginStart? false");
                    Util.sendAndFinish(SplashActivity.this, LoginActivity.class);
                }
            }
        }, 3000);


    }

}
